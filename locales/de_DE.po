# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Janosch, 2015
msgid ""
msgstr ""
"Project-Id-Version: GLPI Project - Plugin Behaviors\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2015-10-01 12:35+0200\n"
"PO-Revision-Date: 2015-10-01 10:38+0000\n"
"Last-Translator: Nelly Mahu-Lasson <nini.lasson@orange.fr>\n"
"Language-Team: German (Germany) (http://www.transifex.com/yllen/glpi-project-plugin-behaviors/language/de_DE/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: de_DE\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: setup.php:99 inc/config.class.php:57
msgid "Behaviours"
msgstr "Verhalten"

#: inc/common.class.php:85 inc/common.class.php:97 inc/common.class.php:117
msgid "Clone"
msgstr "Clone"

#: inc/common.class.php:108 inc/common.class.php:187
msgid "Clone of"
msgstr "Clone von"

#: inc/config.class.php:104
msgid "Error in creating glpi_plugin_behaviors_configs"
msgstr "Fehler beim erstellen von glpi_plugin_behaviors_configs"

#: inc/config.class.php:110
msgid "Error during update glpi_plugin_behaviors_configs"
msgstr "Fehler beim akualisieren von glpi_plugin_behaviors_configs"

#: inc/config.class.php:180
msgid "Inventory"
msgstr "Inventar"

#: inc/config.class.php:184
msgid "Ticket's number format"
msgstr "Format der Ticketnummer"

#: inc/config.class.php:191
msgid "Delete computer in OCSNG when purged from GLPI"
msgstr "Lösche Computer in OCSNG wenn diese in GLPI bereinigt wurden"

#: inc/config.class.php:196
msgid "Plugin \"Item's uninstallation\" not installed"
msgstr "Plugin \"Item's uninstallation\" ist nicht installiert"

#: inc/config.class.php:201
msgid "Use the associated item's group"
msgstr "Verwenden der zugehörigen Gegenstands Gruppe"

#: inc/config.class.php:203
msgid "Notification"
msgid_plural "Notifications"
msgstr[0] "Benachritigung"
msgstr[1] "Benachritigungen"

#: inc/config.class.php:208
msgid "Use the requester's group"
msgstr "Verwenden der Anforderer Gruppe"

#: inc/config.class.php:211
msgid "Additional notifications"
msgstr "Zusätzliche Benachrichtigung"

#: inc/config.class.php:216
msgid "Use the technician's group"
msgstr "Verwenden der Techniker Gruppe"

#: inc/config.class.php:223 inc/ticket.class.php:84
msgid "Requester is mandatory"
msgstr "Anforderer ist verpflichtend"

#: inc/config.class.php:235
msgid "Duration is mandatory before ticket is solved/closed"
msgstr "Die Dauer ist verpflichtend bevor das Ticket geschlossen/gelöst wird"

#: inc/config.class.php:241
msgid "Category is mandatory before ticket is solved/closed"
msgstr "Die Kategorie ist verpflichtend bevor das Ticket gelöst/geschlossen wird"

#: inc/config.class.php:247
msgid "Type of solution is mandatory before ticket is solved/closed"
msgstr "Die Art der Lösung ist verpflichtend bevor das Ticket geschlossen/gelöst wird"

#: inc/config.class.php:254
msgid "Description of solution is mandatory before ticket is solved/closed"
msgstr "Eine Beschreibung der Lösung ist verpflichtend bevor das Ticket geschlossen/gelöst wird"

#: inc/config.class.php:261
msgid "Technician assigned is mandatory before ticket is solved/closed"
msgstr ""

#: inc/config.class.php:268
msgid "Deny change of ticket's creation date"
msgstr "Ändern des Ticket Erstellungsdatums verweigern"

#: inc/config.class.php:273
msgid "Protect from simultaneous update"
msgstr "Vor gleichzeitiger aktualisierung schützen"

#: inc/config.class.php:278
msgid "Single technician and group"
msgstr "Einzelner Techniker und Gruppe"

#: inc/config.class.php:280
msgid "Single user and single group"
msgstr "Einzelner Benutzer und einzelne Gruppe"

#: inc/config.class.php:281
msgid "Single user or group"
msgstr "Einzelner Benutzer oder Gruppe"

#: inc/config.class.php:290
msgid "Type of solution is mandatory before problem is solved/closed"
msgstr "Die Art der Lösung ist verpflichtend bevor das Problem geschlossen/gelöst wird"

#: inc/document_item.class.php:45
msgid "Add document to ticket"
msgstr "Dokument zum Ticket hinzufügen"

#: inc/document_item.class.php:47
msgid "Delete document to ticket"
msgstr "Dokument vom Ticket entfernen"

#: inc/problem.class.php:72
msgid "You cannot close a problem without solution type"
msgstr "Sie können das Problem ohne eine Lösungsart nicht schließen"

#: inc/ticket.class.php:44
msgid "Assign to a technician"
msgstr "Einem Techniker zuweißen"

#: inc/ticket.class.php:45
msgid "Assign to a group"
msgstr "Einer Gruppe zuweißen"

#: inc/ticket.class.php:46
msgid "Reopen ticket"
msgstr "Ticket wiedereröffnen"

#: inc/ticket.class.php:202
msgid "Can't save, item have been updated"
msgstr "Speichern nicht möglich, Gegenstand hat ein Aktualisierung erhalten."

#: inc/ticket.class.php:236
msgid "You cannot solve/close a ticket without duration"
msgstr ""

#: inc/ticket.class.php:245
msgid "You cannot solve/close a ticket without solution type"
msgstr ""

#: inc/ticket.class.php:254
msgid "You cannot solve/close a ticket without solution description"
msgstr ""

#: inc/ticket.class.php:263
msgid "You cannot solve/close a ticket without ticket's category"
msgstr ""

#: inc/ticket.class.php:273
msgid "You cannot solve/close a ticket without techician assign"
msgstr ""
